const express = require ('express');
const PORT = process.env.PORT || 4000;
const app = express();
const cors = require('cors');

let result = '';

app.use( express.json() );
app.use( express.urlencoded( {extended:true} ) );
app.use( cors() );

app.post('/', (req, res) => {
 	result = req.body;
 	return res.send(result);
})

app.get('/result', (req, res) => {
  	return res.send(result);
})


app.get('/', (req, res) => {
   	return res.send('Hello World!')
})

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`)
})